// parameters
const elBankBalance = document.getElementById('bank-balance');
const elGetLoanBtn = document.getElementById('btn-get-a-loan');
const elWorkBalance = document.getElementById('work-balance')
const elWorkBtn = document.getElementById('btn-work');
const elBankMoneyBtn = document.getElementById('btn-bank-money');
const elLaptopList = document.getElementById('laptops');
const elLaptopFeatureList = document.getElementById('laptop-features');
const elBuyNowBtn = document.getElementById('btn-buy');
const elPictureBox = document.getElementById('picture');






// Bank Section
let counter = 0;
let bankBalance = 0;

// let laptops = [];

elGetLoanBtn.addEventListener('click', function(event) {
    let loan = prompt('How big of a loan do you want?');
    if(loan === null || loan === ''){
        loan = 0;
        return;
    }
    takeLoan(parseInt(loan));
    elBankBalance.innerText = bankBalance;
   
})

// function to take a loan
function takeLoan(amount=0) {
    if(amount > 2*bankBalance) {
        alert('Amount must be smaller than two times the bank balance');
        return;
    }
    if(counter >= 1){
        alert('Can only make one loan before buying a computer');
        return;
    }
    bankBalance += amount;
    counter++;
}








// Work Section
let workBalance = 0;

elWorkBtn.addEventListener('click', function(event) {
    work();
    elWorkBalance.innerText = workBalance;
   
})

elBankMoneyBtn.addEventListener('click', function(event) {
    depositWorkBalance();
    elWorkBalance.innerText = workBalance;
    
    elBankBalance.innerText = bankBalance;
    
})

// function to add 100 kroner to work balance
function work() {
    workBalance += 100;
}

// function to deposit money to bank balance from work balance
function depositWorkBalance() {
    bankBalance += workBalance;
    workBalance = 0;
}







// Laptop Section

const laptop1 = {
    name: 'Mac Awesome',
    price: 5000,
    description: 'This Mac is awesome! All our other macs are also truly awesome, so you will probably be happy with whichever you choose ;)',
    featureList: ['Has a keyboard', 'Has a screen', 'Has a touchpad', '...and much more!'],
    imageLink: 'mac.jpg'
}
const laptop2 = {
    name: 'Asus POWERhouse',
    price: 4500,
    description: 'This is a computer for the heavy simulater! This computer is all about POWER!! If you are weak, this computer will make you powerful nonetheless.',
    featureList: ['Immense power', 'Power', 'More power', 'Power storage'],
    imageLink: 'asus.jpg'
}

const laptop3 = {
    name: 'Acer Gaming Laptop',
    price: 6000,
    description: 'Acer Gaming Laptop provides you with a laptop ready for any game you want to play! With a state of the art graphic card and everything else a gamping laptop needs.',
    featureList: ['Great graphic card', 'Lots of RAM', 'Nice screen for pretty graphics'],
    imageLink: 'acer.jpg'
}

const laptop4 = {
    name: 'hp Work Computer',
    price: 2500,
    description: 'Do you need a computer for work? Look no more! hp Work Computer provides you with everything necessary to work!',
    featureList: ['A nice keyboard', 'A screen where information can be seen', 'Touchpad for your pretty fingers', 'Pre-installed MS Office'],
    imageLink: 'hp.jpg'
}

const laptops = [laptop1, laptop2, laptop3, laptop4];


elLaptopList.addEventListener('click', function(event) {
    elLaptopFeatureList.innerHTML = '';
    laptops[elLaptopList.value].featureList.forEach(feature => {
        const elListItem = document.createElement('li');
        elListItem.innerText = feature;
        elLaptopFeatureList.appendChild(elListItem);
        document.getElementById('laptop-description').innerText = laptops[elLaptopList.value].description;
        document.getElementById('laptop-title').innerText = laptops[elLaptopList.value].name;
        document.getElementById('laptop-price').innerText = `${laptops[elLaptopList.value].price}kr`;
    });
    elPictureBox.innerHTML = '';
    const elPicture = document.createElement('img');
    elPicture.src = laptops[elLaptopList.value].imageLink;
    elPicture.style.width = "150px";
    elPicture.style.height = "120px";
    elPictureBox.appendChild(elPicture);
})

elBuyNowBtn.addEventListener('click', function(event) {
    if(checkBalance()){
        bankBalance -= parseInt(laptops[elLaptopList.value].price);
        alert(`You got yourself a new ${laptops[elLaptopList.value].name}!`);
        counter = 0;
        render();
        return;
    }
    alert(`You can't afford this computer :(`);
})

function checkBalance(){
    return bankBalance >= parseInt(laptops[elLaptopList.value].price);
}

function render() {
    elBankBalance.innerText = bankBalance;
    elWorkBalance.innerText = workBalance;

    elLaptopFeatureList.innerHTML = '';
    laptops[elLaptopList.value].featureList.forEach(feature => {
        const elListItem = document.createElement('li');
        elListItem.innerText = feature;
        elLaptopFeatureList.appendChild(elListItem);
        document.getElementById('laptop-description').innerText = laptops[elLaptopList.value].description;
        document.getElementById('laptop-title').innerText = laptops[elLaptopList.value].name;
        document.getElementById('laptop-price').innerText = `${laptops[elLaptopList.value].price}kr`;
    })
}